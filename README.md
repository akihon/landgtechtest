<h1>Legal & General Technical Test</h1>

<h3>To run</h3> 

Navigate to the /target folder and issue the following 

java -jar landg-tech-test-1.0-SNAPSHOT.jar <<0.00>> 

The <<0.00>> is the initial amount to start the vending machine 
with a given float amount, float in this case being an amount to use 
for change handling. 

<h3>Design choices</h3>

Taken from the requirements I went with a simple input calculate output 
system using the terminal interface as the main way to interact with the vending machine 

