package com.application;

import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        VendingMachineConfiguration config = new VendingMachineConfigurationImpl();
        VendingMachine machine = new VendingMachine();
        if (args[0] != null) {
            config.setUpMachine(machine, Double.valueOf(args[0]));
        } else {
            System.out.println("Cannot start the machine, need an initial amount as a float");
            System.exit(1);
        }

        //allow a user to check items
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("To see a list of products, enter L or exit to quit ");
            String input = scanner.nextLine();
            if (input.equals("exit")) {
                System.exit(1);
            }

            if (input.equals("L")) {
                System.out.println("Here is a list of what's available");
                int index = 1;
                for (Map.Entry<String, Double> s : machine.getProducts().entrySet()) {
                    System.out.println(index + " " + s.getKey() + " " + s.getValue());
                    index++;
                }
            }

            System.out.println("To make a selection please enter a number press enter, type done and enter when finished");
            while(!input.equals("done")) {
                int selection = Integer.valueOf(input);
                CurrentSelection currentSelection = new CurrentSelection();

                if(selection >= 1 || selection <= machine.getProducts().size()) {
                    //add to the current selection 
                    System.out.println("Here is your selection so far");

                } else {
                    System.out.println("There is no item with that number value, please enter another number");
                }
            }


            //print out the selection and the current total


        }

    }
}
