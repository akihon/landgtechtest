package com.application;

import java.util.HashMap;
import java.util.Map;

public class Products {

    public static Map<String, Double> productsMap;

    static {
        productsMap = new HashMap<String, Double>();
        productsMap.put("Snickers", 1.00);
        productsMap.put("Coke", 1.00);
        productsMap.put("Salt and Vinegar Crisps", 0.80);
    }
}
