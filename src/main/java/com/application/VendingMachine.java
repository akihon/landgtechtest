package com.application;

import java.util.Map;

public class VendingMachine {

    private double initialAmount;
    private Map<String, Double> products;

    public void setup(Double valueOf) {
        this.initialAmount = valueOf;
        products = Products.productsMap;
    }

    public Map<String, Double> getProducts() {
        return products;
    }
}
