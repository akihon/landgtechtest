package com.application;

import java.util.HashMap;
import java.util.Map;

public class CurrentSelection {

    private Map<String, Double> customerSelection;

    public CurrentSelection() {
        this.customerSelection = new HashMap<>();
    }

    public Map<String, Double> getCustomerSelection() {
        return customerSelection;
    }

    public void addToSelection(String key, Double value) {
        customerSelection.put(key, value);
    }
}
