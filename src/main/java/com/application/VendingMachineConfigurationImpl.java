package com.application;

public class VendingMachineConfigurationImpl implements VendingMachineConfiguration {

    public void setUpMachine(VendingMachine machine, Double valueOf) {
        machine.setup(valueOf);
    }
}
