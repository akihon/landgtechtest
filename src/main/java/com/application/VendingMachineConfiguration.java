package com.application;

public interface VendingMachineConfiguration {

    void setUpMachine(VendingMachine machine, Double valueOf);
}
